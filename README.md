# Pinocchio examples
![](https://github.com/stack-of-tasks/pinocchio/raw/master/doc/images/pinocchio-logo-large.png)
## Download the examples

### By using the terminal:

```bash
git clone https://gitlab.inria.fr/auctus-team/people/antunskuric/example/pinocchio_examples.git
```
### By zip download
Download this repository amd unzip it from the link 
https://gitlab.inria.fr/auctus-team/people/antunskuric/example/pinocchio_examples


## Installing the environment
The simplest way to install these python libraries is using anaconda.
```bash
conda env create -f env.yaml    # create the new environemnt and install pinocchio, gepetto, pycapacity,.. 
conda actiavte pio_examples
```

### creating a custom envirnoment
You can also simply use anaconda to create a new custom environment:
```bash
conda create -n pio_examples python=3.8 pip # create python 3.8 based environment
conda actiavte pio_examples
```

Install all the needed packages
```bash
conda install -c conda-forge pinocchio 
conda install -c conda-forge example-robot-data 
conda install -c conda-forge gepetto-viewer 
```

Then install `pycapacity` for the workspace analysis
```bash
pip install pycapacity
```

## Running the examples
Panda3D visualiser test:
```
python test_panda3d.py
```

Gepetto viewer visualiser test:
```
gepetto-gui&python test_gepetto_polytopes.py
```
